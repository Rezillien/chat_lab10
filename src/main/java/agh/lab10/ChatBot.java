package agh.lab10;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONObject;

public class ChatBot {
    private final static String apiid = "a7491d207da86b9781db70fa4bf6b6ea";
    private final static String townID = "3094802";

    public ChatBot(){

    }

    public static String getAnswer(String question){
        switch(question){
            case "time":
                return getHour();
            case "day":
                return getDay();
            case "weather":
                return getWeather();
            default:
                return "possible questions:"
                        + "\ntime\nday\nweather   -in Cracow";
        }
    }

    private static String getWeather() {
        String url="http://api.openweathermap.org/data/2.5/weather?id=" + townID + "&APPID=" + apiid;
        StringBuilder weather = new StringBuilder();
        try(BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        new URL(url).openStream(), Charset.forName("UTF-8")));){
            for(String line ; ( line = br.readLine() ) != null ; ){
                weather.append(line);
            }

            JSONObject jsonWeather = new JSONObject(weather.toString());

            String main = jsonWeather.getJSONArray("weather").getJSONObject(0).getString("main");
            String temp = jsonWeather.getJSONObject("main").getDouble("temp") + "";

            double dTemp=Double.parseDouble(temp);
            dTemp-=273f;

            return main+" temperature: " + Double.toString(Math.round(dTemp*100)/100);
        }
        catch(Exception ex){

        }


        return "nie udalo sie pobrac informacji o pogodzie";
    }

    private static String getHour() {
        return new SimpleDateFormat("HH:mm:ss").format(new Date());

    }

    private static String getDay(){
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        // 3 letter name form of the day
        return new SimpleDateFormat("EE", Locale.ENGLISH).format(date.getTime());
    }

}
